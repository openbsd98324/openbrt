# openbrt

openbrt is a barebone, nano linux for routers and for LFS. 


## Copy the Barebone on your Partition 

Example: target is sda1

mkfs.ext3 -F /dev/sda1 

mkdir -p /media/sda1

mount /dev/sda1 /media/sda1 

cd /media/sda1 

wget -c --no-check-certificate https://gitlab.com/openbsd98324/openbrt/-/raw/main/rootfs/linux-openbrt-rootfs-barebone-v1.tar.gz

tar xvpfz linux-openbrt-rootfs-barebone-v1.tar.gz


reboot



## Usage

Once the linux barebone is booted, you will have an interpreter (started by /sbin/init).
You can type the following command line: 

bsh


Afterwards, bsh will host your commands. Example: 

cmd whateversoftware


Another example:

cd /usr/local/bin

cmd davepanel 



## Build LFS BASE

Build then your linux.





## GRUB 

The grub grub.cfg can contain: 

  
 menuentry 'Linux Devuan X11, Devuan on sda1 (tm, stable)' --class devuan --class gnu-linux --class gnu { 

        insmod gzio

        insmod part_msdos

        insmod ext2

        set root='hd0,msdos1'

        linux   /boot/vmlinuz-4.9.0-11-amd64 root=/dev/sda1  rw 

        initrd  /boot/initrd.img-4.9.0-11-amd64

}







